# BugAudit Scanner - Brakeman
[![Download](https://api.bintray.com/packages/bugaudit/maven/bugaudit-scanner-brakeman/images/download.svg)](https://bintray.com/bugaudit/maven/bugaudit-scanner-brakeman/_latestVersion)
[![Build Status](https://gitlab.com/bugaudit/bugaudit-scanner-brakeman/badges/master/pipeline.svg)](https://gitlab.com/bugaudit/bugaudit-scanner-brakeman/pipelines)

A bug audit scanner using brakeman for Ruby
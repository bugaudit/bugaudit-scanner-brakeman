package me.shib.bugaudit.scanner.ruby.brakeman;

import me.shib.bugaudit.commons.BugAuditContent;
import me.shib.bugaudit.commons.BugAuditException;
import me.shib.bugaudit.scanner.Bug;
import me.shib.bugaudit.scanner.BugAuditScanner;
import me.shib.bugaudit.scanner.Lang;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public final class BrakemanScanner extends BugAuditScanner {

    private static final String tool = "Brakeman";
    private static final File brakemanOutput = new File("bugaudit-brakeman-result.json");
    private static final String brakemanConfigPath = System.getenv("BUGAUDIT_SCANNER_BRAKEMAN_CONFIG");
    private static final String brakemanIgnorePath = System.getenv("BUGAUDIT_SCANNER_BRAKEMAN_IGNORE");
    private static final String[] excludedPaths = {"Gemfile.lock"};
    private static final Map<String, Integer> typeToPriorityMap;

    static {
        typeToPriorityMap = new HashMap<>();
        typeToPriorityMap.put("Attribute Restriction", 4);
        typeToPriorityMap.put("Authentication", 2);
        typeToPriorityMap.put("Basic Authentication", 2);
        typeToPriorityMap.put("Command Injection", 2);
        typeToPriorityMap.put("Cross-Site Request Forgery", 3);
        typeToPriorityMap.put("Cross Site Scripting", 2);
        typeToPriorityMap.put("Cross Site Scripting (Content Tag)", 2);
        typeToPriorityMap.put("Cross Site Scripting (JSON)", 3);
        typeToPriorityMap.put("Dangerous Evaluation", 2);
        typeToPriorityMap.put("Dangerous Send", 3);
        typeToPriorityMap.put("Default Routes", 4);
        typeToPriorityMap.put("Denial of Service", 2);
        typeToPriorityMap.put("Dynamic Render Paths", 4);
        typeToPriorityMap.put("File Access", 3);
        typeToPriorityMap.put("Format Validation", 4);
        typeToPriorityMap.put("Information Disclosure", 4);
        typeToPriorityMap.put("Mail Link", 2);
        typeToPriorityMap.put("Mass Assignment", 3);
        typeToPriorityMap.put("Remote Code Execution", 2);
        typeToPriorityMap.put("Remote Execution in YAML.load", 2);
        typeToPriorityMap.put("Session Manipulation", 3);
        typeToPriorityMap.put("Session Settings", 3);
        typeToPriorityMap.put("SQL Injection", 2);
        typeToPriorityMap.put("SSL Verification Bypass", 3);
        typeToPriorityMap.put("Unsafe Deserialization", 2);
        typeToPriorityMap.put("Unscoped Find", 3);
        typeToPriorityMap.put("Unsafe Redirects", 3);
    }

    public BrakemanScanner() throws BugAuditException {
        super();
        this.getBugAuditScanResult().setTypeToPriorityMap(typeToPriorityMap);
        this.getBugAuditScanResult().addKey("SAST-Warning");
    }

    private static int getPriorityForConfidence(String confidence) {
        switch (confidence) {
            case "Urgent":
                return 1;
            case "High":
                return 2;
            case "Medium":
                return 3;
            case "Weak":
                return 4;
            default:
                return 3;
        }
    }

    private static boolean isExcludedPath(String path) {
        for (String p : excludedPaths) {
            if (p.equalsIgnoreCase(path)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean isLangSupported(Lang lang) {
        return lang == Lang.Ruby;
    }

    @Override
    public String getTool() {
        return tool;
    }

    @Override
    public void scan() throws BugAuditException, IOException, InterruptedException {
        if (!isParserOnly()) {
            brakemanOutput.delete();
            runBrakeman();
        }
        processBrakemanResult();
    }

    private void runBrakeman() throws BugAuditException, IOException, InterruptedException {
        StringBuilder brakemanCommandBuilder = new StringBuilder();
        brakemanCommandBuilder.append("brakeman -o ").append(brakemanOutput.getAbsolutePath());
        if (brakemanConfigPath != null && !brakemanConfigPath.isEmpty()) {
            File brakemanConfig = new File(brakemanConfigPath);
            if (brakemanConfig.exists()) {
                brakemanCommandBuilder.append(" -c ").append(brakemanConfig.getPath());
            }
        }
        if (brakemanIgnorePath != null && !brakemanIgnorePath.isEmpty()) {
            File brakemanIgnore = new File(brakemanIgnorePath);
            if (brakemanIgnore.exists()) {
                brakemanCommandBuilder.append(" -i ").append(brakemanIgnore.getPath());
            }
        }
        String response = runCommand(brakemanCommandBuilder.toString());
        if (response.contains("command not found") || response.contains("is currently not installed")) {
            throw new BugAuditException("Install brakeman before proceeding");
        }
    }

    private Bug warningToBug(BrakemanWarning warning) throws BugAuditException {
        String title = "Brakeman warning (" + warning.getWarning_type() + ") found in " + warning.getFile() + " of " + getBugAuditScanResult().getRepo();
        Bug bug = new Bug(title, getPriorityForConfidence(warning.getConfidence()));
        bug.setDescription(new BugAuditContent(getDescription(warning)));
        bug.addType(warning.getWarning_type().replace(" ", "-"));
        addKeys(bug, warning);
        return bug;
    }

    private String getDescription(BrakemanWarning warning) {
        StringBuilder description = new StringBuilder();
        description.append("The following insecure code warnings were found in ").append("**[")
                .append(warning.getFile()).append("](").append(getBugAuditScanResult().getRepo().getWebUrl()).append("/tree/")
                .append(getBugAuditScanResult().getRepo().getCommit()).append("/").append(warning.getFile()).append("):**\n");
        description.append(" * **Line:** ").append(warning.getLine()).append("\n");
        description.append(" * **Type:** ");
        if (warning.getLink() != null) {
            description.append("[").append(warning.getWarning_type()).append("](").append(warning.getLink()).append(")");
        } else {
            description.append(warning.getWarning_type());
        }
        description.append("\n");
        description.append(" * **Message:** ").append(warning.getMessage()).append("\n");
        description.append(" * **Confidence:** ").append(warning.getConfidence());
        if (warning.getCode() != null) {
            description.append("\n * **Code:**\n");
            description.append("```\n");
            description.append(warning.getCode());
            description.append("\n```");
        }
        return description.toString();
    }

    private void addKeys(Bug bug, BrakemanWarning warning) throws BugAuditException {
        bug.addKey(warning.getFile());
        bug.addKey(getBugAuditScanResult().getRepo() + "-" + warning.getFingerprint());
    }

    private void processBrakemanResult() throws IOException, BugAuditException {
        BrakemanResult brakemanResult = BrakemanResult.getBrakemanResult(brakemanOutput);
        for (BrakemanWarning warning : brakemanResult.getWarnings()) {
            if (!isExcludedPath(warning.getFile())) {
                getBugAuditScanResult().addBug(warningToBug(warning));
            }
        }
    }

}
